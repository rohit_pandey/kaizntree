Rails.application.routes.draw do
 
  scope module: 'account_block' do
    resources :accounts, :only => [:create]
    scope module: 'accounts' do
      resources :sms_confirmations, :only => [:create]
    end
  end

  scope module: 'bx_block_login' do
    resources :logins, :only => [:create]
  end

   namespace :bx_block_dashboard do
    resources :dashboards
   end

 

  scope module: 'bx_block_forgot_password' do
    resources :otps, :only => [:create]
    resources :otp_confirmations, :only => [:create]
    resources :passwords, :only => [:create]
  end
end
