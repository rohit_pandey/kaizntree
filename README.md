# **Ruby** | _**ManagePhonebookEnterpri**_ | _**194923** | _**studio_pro**_

## **Catalog ProjectId: 118837** | **Catalog BuildId: 5777**

## NOTE FOR DEVELOPERS:
Clone the code-engine branch into your working branch. The contents of the branch may get overwritten.
## Author:
Code-Engine
## Assembled Features To Block Status

| **Feature-Name**        | **Block-Name**        | **Path**  | **Status**  |
|:-------------|:-------------|:-------------|:-------------|
| BulkUploading      | bx_block_bulk_uploading<br>      | {+app/controllers/bx_block_bulk_uploading+}<br> | {+Non-Empty+} |
| SignuploginModule2      | bx_block_login<br>bx_block_forgot_password<br>account_block<br>      | {+app/controllers/bx_block_login+}<br>{+app/controllers/bx_block_forgot_password+}<br>{+app/controllers/account_block+}<br> | {+Non-Empty+} |
| AdminConsole3      | bx_block_admin<br>      | {+app/controllers/bx_block_admin+}<br> | {+Non-Empty+} |
| RolesPermissions2      | bx_block_roles_permissions<br>      | {+app/controllers/bx_block_roles_permissions+}<br> | {+Non-Empty+} |
| Profilebio      | bx_block_profile<br>bx_block_profile_bio<br>      | {+app/controllers/bx_block_profile+}<br>{+app/controllers/bx_block_profile_bio+}<br> | {+Non-Empty+} |
| PhoneLogin      | bx_block_login<br>bx_block_forgot_password<br>      | {+app/controllers/bx_block_login+}<br>{+app/controllers/bx_block_forgot_password+}<br> | {+Non-Empty+} |
| CategoriessubCategories      | bx_block_categories<br>      | {+app/controllers/bx_block_categories+}<br> | {+Non-Empty+} |
| Settings5      | bx_block_settings<br>      | {+app/controllers/bx_block_settings+}<br> | {+Non-Empty+} |
| Notifications3      | bx_block_notifications<br>bx_block_push_notifications<br>      | {+app/controllers/bx_block_notifications+}<br>{+app/controllers/bx_block_push_notifications+}<br> | {+Non-Empty+} |
| TwofactorAuthentication2      | account_block<br>bx_block_forgot_password<br>      | {+app/controllers/account_block+}<br>{+app/controllers/bx_block_forgot_password+}<br> | {+Non-Empty+} |
| GeolocationReporting2      | bx_block_location<br>      | {+app/controllers/bx_block_location+}<br> | {+Non-Empty+} |
| FriendList      | bx_block_friendlist      | {-app/controllers/bx_block_friendlist-} | {-Empty-} |
| PrivateChat      | bx_block_privatechat      | {-app/controllers/bx_block_privatechat-} | {-Empty-} |
| Gallery      | bx_block_gallery      | {-app/controllers/bx_block_gallery-} | {-Empty-} |
| LanguageSupport      | bx_block_languagesupport      | {-app/controllers/bx_block_languagesupport-} | {-Empty-} |
| ElasticSearch      | bx_block_elasticsearch      | {-app/controllers/bx_block_elasticsearch-} | {-Empty-} |
| ContactsList      | bx_block_contactslist      | {-app/controllers/bx_block_contactslist-} | {-Empty-} |
| LocationbasedAlerts      | bx_block_locationbasedalerts      | {-app/controllers/bx_block_locationbasedalerts-} | {-Empty-} |
| GoogleContactsImport2      | bx_block_googlecontactsimport2      | {-app/controllers/bx_block_googlecontactsimport2-} | {-Empty-} |
| Geofence2      | bx_block_geofence2      | {-app/controllers/bx_block_geofence2-} | {-Empty-} |

## GRAFANA BACKEND URL
 - https://grafana.managephonebookenterpri-194923-ruby.b194923.dev.eastus.az.svc.builder.cafe

This application is a Ruby API. Full README coming soon...