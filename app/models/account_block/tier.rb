module AccountBlock
  class Tier < ApplicationRecord
    self.table_name = :tiers
    has_many :sections, class_name: 'AccountBlock::Section'
    has_many :accounts, through: :sections, class_name: 'AccountBlock::Account',dependent: :destroy
    has_many :updated_tiers, class_name: 'AccountBlock::UpdatedTier'
    validates :unique_name, uniqueness: true
  end
end
