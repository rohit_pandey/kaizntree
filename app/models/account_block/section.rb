module AccountBlock
  class Section < ApplicationRecord
    self.table_name = :sections
    belongs_to :account
    belongs_to :tier
  end
end