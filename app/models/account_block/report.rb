module AccountBlock
  class Report < ApplicationRecord
    self.table_name = :reports
    belongs_to :account, class_name: "AccountBlock::Account"


    SCOPES = [:contains, :equals, :starts_with, :ends_with]
    
    SCOPES.each do |condition_type|
    scope "first_name_#{condition_type}", lambda { |q|
      joins(:account).merge AccountBlock::Account.search("first_name_#{condition_type}" => q).result
    }
    end

    def self.ransackable_scopes(auth_object = nil)
      SCOPES.map{ |k| "first_name_#{k}" }
    end
  end
end