module AccountBlock
  class ContactList < ApplicationRecord
    self.table_name = :contact_list
    belongs_to :account
  end
end
