module AccountBlock
  class SmsAccount < Account
    include Wisper::Publisher
    validates :full_phone_number, presence: true
  end
end
