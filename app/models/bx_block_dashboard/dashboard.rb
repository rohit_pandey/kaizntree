module BxBlockDashboard
  class Dashboard < ApplicationRecord
    self.table_name = :dashboards
    
    enum tag: ["k12", "higher_education", "govt_job", "competitive_exams", "upskilling"]
    enum category: ["bundles", "finished_products", "raw_materials", "new_products"]
   
  end
end
