module AccountBlock
  class SmsOtpSerializer < BuilderBase::BaseSerializer

    attribute :account_id do |object|
       AccountBlock::Account.find_by(full_phone_number: object.full_phone_number).id    
    end
  end
end
