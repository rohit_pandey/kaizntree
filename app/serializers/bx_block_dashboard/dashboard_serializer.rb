module BxBlockDashboard
  class DashboardSerializer < BuilderBase::BaseSerializer
    attributes *[
      :sku,
      :name,
      :tag,
      :category,
      :in_stock,
      :available_stock,
      :created_at,
      :updated_at,
    ]
  end
end
