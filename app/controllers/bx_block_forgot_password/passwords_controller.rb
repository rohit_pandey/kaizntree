module BxBlockForgotPassword
  class PasswordsController < ApplicationController
    def create
      if request.headers[:token].present? && create_params[:new_password].present?
        # Try to decode token with OTP information
        return render json: {
          error: {
            message: "Please confirm new password",
          },
        }, status: :unprocessable_entity unless create_params[:new_password_confirmation].present?
        return render json: {
          error: {
            message: "New password and confirm new password doesn't match",
          },
        }, status: :unprocessable_entity unless create_params[:new_password] == create_params[:new_password_confirmation]
        begin
          token = BuilderJsonWebToken.decode(request.headers[:token])
        rescue JWT::DecodeError => e
          return render json: {
            error: {
              message: 'Invalid token',
            },
          }, status: :bad_request
        end

        # Try to get OTP object from token
        begin
        
         
          otp = token.type.constantize.find(token.id)
        rescue ActiveRecord::RecordNotFound => e
          return render json: {
            error: {
              message: 'Token invalid',
            },
          }, status: :unprocessable_entity
        end

        # Check if OTP was validated
        unless otp.activated?
          return render json: {
            error: {
              message: 'OTP code not validated',
            },
          }, status: :unprocessable_entity
        else
          # Check new password requirements
          password_validation = AccountBlock::PasswordValidation
            .new(create_params[:new_password])

          is_valid = password_validation.valid?
          error_message = 'Password should be 8 characters long, must contain one uppercase letter, one special charater and one number.'

          unless is_valid
            return render json: {
              error: {
                message: error_message,
              },
            }, status: :unprocessable_entity
          else
            # Update account with new password
            account = AccountBlock::SmsAccount.find(token.account_id)

            if account.update(:password => create_params[:new_password])
              # Delete OTP object as it's not needed anymore
              otp.destroy

              serializer = AccountBlock::SmsAccountSerializer.new(account)
              serialized_account = serializer.serializable_hash

              render json: serialized_account, status: :created
            else
              render json: {
                error: {
                  message: 'Password change failed',
                },
              }, status: :unprocessable_entity
            end
          end
        end
      else
        return render json: {
          error: {
            message: 'Please enter a new password',
          },
        }, status: :unprocessable_entity
      end
    end

    private

    def create_params
      params.require(:data)
        .permit(*[
          :new_password,
          :new_password_confirmation
        ])
    end
  end
end
