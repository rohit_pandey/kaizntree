module BxBlockForgotPassword
  class OtpConfirmationsController < ApplicationController
    def create
      if request.headers[:token].present? && create_params[:otp_code].present?
        # Try to decode token with OTP information
        begin
          token = BuilderJsonWebToken.decode(request.headers[:token])
        rescue JWT::ExpiredSignature
          return render json: {
            error: {
              message: 'OTP has expired, please request a new one.',
            },
          }, status: :unauthorized
        rescue JWT::DecodeError => e
          return render json: {
            error: {
              message: 'Invalid token',
            },
          }, status: :bad_request
        end

        # Try to get OTP object from token
        begin
          otp = token.type.constantize.find(token.id)
        rescue ActiveRecord::RecordNotFound => e
          return render json: {
            error: {
              message: 'Token invalid',
            },
          }, status: :unprocessable_entity
        end

        # Check OTP code
        if 1234 == create_params[:otp_code].to_i
          otp.activated = true
          otp.save
          if create_params[:type] == 'login'
            render json: {meta: {
              token: BuilderJsonWebToken.encode(create_params['account_id'], 1.day.from_now, token_type: 'login'),
              refresh_token: BuilderJsonWebToken.encode(create_params['account_id'], 1.year.from_now, token_type: 'refresh'),
              id: token.account_id
            }}
          else
            render json: {
              meta: {
                otp: 'OTP validation success',
                #token: request.headers[:token],
                token: BuilderJsonWebToken.encode(create_params['account_id'], 1.day.from_now, token_type: 'email_login',id: token.account_id),
                account_id: token.account_id
              
              },
            }, status: :created
          end
        else
          return render json: {
            error: {
              message: 'Invalid OTP code',
            },
          }, status: :unprocessable_entity
        end
      else
        return render json: {
          error: {
            message: 'Token and OTP code are required',
          },
        }, status: :unprocessable_entity
      end
    end

    private

    def create_params
      params.require(:data)
        .permit(*[
          :otp_code,
          :type,
          :account_id
        ])
    end
  end
end
