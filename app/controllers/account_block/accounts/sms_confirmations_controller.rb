# frozen_string_literal: true

module AccountBlock
  module Accounts
    class SmsConfirmationsController < ApplicationController
      include BuilderJsonWebToken::JsonWebTokenValidation

      before_action :validate_json_web_token

      def create
        begin
          @sms_otp = SmsOtp.find(@token.id)
        rescue ActiveRecord::RecordNotFound => e
          return render json: {error: 
            {message: 'Phone Number Not Found'},
          }, status: :unprocessable_entity
        end
         
        if @sms_otp.valid_until < Time.current
          @sms_otp.destroy

          return render json: {error: 
            {message: 'This Pin has expired, please request a new pin code.'},
          }, status: :unauthorized
        end

        if @sms_otp.activated?
          return render json: ValidateAvailableSerializer.new(@sms_otp, meta: {
            message: 'Phone Number Already Activated',
          }).serializable_hash, status: :ok
        end

        if "1234" == params['pin'].to_s
          @sms_otp.activated = true
          if @sms_otp.save
            if params[:type] == 'login'
              render json: {meta: {
                token: BuilderJsonWebToken.encode(@token.account_id, 1.day.from_now, token_type: 'login'),
                refresh_token: BuilderJsonWebToken.encode(@token.account_id, 1.year.from_now, token_type: 'refresh'),
                id: @token.account_id
              }}
            else
              account = AccountBlock::SmsAccount.find(@token.account_id)
              # cometchat_user = BxBlockChat::Cometchat::User.new(account.user_name, account).create_user
               # account.update(activated: true)
              # account.update(activated: true, user_name: cometchat_user["data"]["uid"])
              render json: SmsAccountSerializer.new(account, meta: {
                token: BuilderJsonWebToken.encode(account.id)
              }).serializable_hash, status: :created
            end
          end
          # render json: ValidateAvailableSerializer.new(@sms_otp, meta: {
          #   message: 'Phone Number Confirmed Successfully',
          #   token: BuilderJsonWebToken.encode(@sms_otp.id),
          # }).serializable_hash, status: :ok
        else
          return render json: {error: 
            {message: 'Invalid Pin for Phone Number'},
          }, status: :unprocessable_entity
        end
      end
    end
  end
end


