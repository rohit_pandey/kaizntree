module AccountBlock
  class AccountsController < ApplicationController
    include BuilderJsonWebToken::JsonWebTokenValidation

    before_action :validate_json_web_token, only: [:search, :deactivate_account]

    def create
      case params[:data][:type] #### rescue invalid API format
      when 'sms_account'
        account_params = jsonapi_deserialize(params)
        account_params["email"] = account_params["email"].downcase if account_params["email"].present?
        sms_account = SmsAccount.where(full_phone_number: account_params["full_phone_number"][1..-1], activated: true).first
        return render json: {error: 
          {message: 'Phone number already associated with another account'},
        }, status: :unprocessable_entity if sms_account.present?

        if account_params["email"].present?
          email_valid = AccountBlock::EmailValidation.new(account_params["email"])
          return render json: {error: 
            {message: "email is not valid"},
          }, status: :unprocessable_entity unless email_valid.valid?

          email_account = SmsAccount.where(email: account_params["email"], activated: true).first
        
          return render json: {error: 
            {message: 'Email already associated with another account'},
          }, status: :unprocessable_entity if email_account.present?
        end

        password_validation = AccountBlock::PasswordValidation.new(account_params["password"])

        is_valid = password_validation.valid?
        error_message = 'Password should be 8 characters long, must contain one uppercase letter, one special charater and one number.'

        return render json: {error: 
          {message: error_message},
        }, status: :unprocessable_entity unless is_valid

        @account = SmsAccount.new(account_params)
          if @account.save
            @sms_otp = SmsOtp.new(full_phone_number: account_params["full_phone_number"])
            if @sms_otp.save
              @account.update(activated: true)
              render json: SmsOtpSerializer.new(@sms_otp, meta: {
                token: BuilderJsonWebToken.encode(@sms_otp.id, account_id: @account.id)
              }).serializable_hash, status: :created
            else
              render json: {error: {message: @sms_otp.errors.full_messages.slice(0)}},
                status: :unprocessable_entity
            end
           
          else
            render json: {error: {message: @account.errors.full_messages.slice(0)}},
              status: :unprocessable_entity
          end
       

      when 'email_account'
        account_params = jsonapi_deserialize(params)

        query_email = account_params['email'].downcase
        account = EmailAccount.where('LOWER(email) = ?', query_email).first

        validator = EmailValidation.new(account_params['email'])

        return render json: {errors: [
          {account: 'Email invalid'},
        ]}, status: :unprocessable_entity if account || !validator.valid?

        @account = EmailAccount.new(jsonapi_deserialize(params))
        @account.platform = request.headers['platform'].downcase if request.headers.include?('platform')
          #byebug
        if @account.save
          EmailValidationMailer
            .with(account: @account, host: request.base_url)
            .activation_email.deliver
          render json: EmailAccountSerializer.new(@account, meta: {
            token: encode(@account.id),
          }).serializable_hash, status: :created
        else
          # render json: {errors: format_activerecord_errors(@account.errors)},
          #   status: :unprocessable_entity
           render json: {error: {message: @account.errors.full_messages.slice(0)}},
              status: :unprocessable_entity
        end

     
      else
        render json: {error: 
          {message: 'Invalid Account Type'},
        }, status: :unprocessable_entity
      end
    end

    def search
      @accounts = Account.where(activated: true)
                    .where('first_name ILIKE :search OR '\
                           'last_name ILIKE :search OR '\
                           'email ILIKE :search', search: "%#{search_params[:query]}%")
      if @accounts.present?
        render json: AccountSerializer.new(@accounts, meta: {message: 'List of users.'
        }).serializable_hash, status: :ok
      else
        render json: {errors: [{message: 'Not found any user.'}]}, status: :ok
      end
    end

    private

    def encode(id)
      BuilderJsonWebToken.encode id
    end

    def search_params
      params.permit(:query)
    end
  end
end
