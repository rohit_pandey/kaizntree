module BxBlockDashboard
  class DashboardsController < ApplicationController

   skip_before_action :validate_json_web_token, only: [:index]
   before_action :find_dashboards, only: %i[index]
     

    
    def create
      dashboard = BxBlockDashboard::Dashboard.new(dashboard_params)

      if dashboard .save
        render json: BxBlockDashboard::DashboardSerializer.new(dashboard)
                                                       .serializable_hash,
               status: :created
      else
        render json: { errors: dashboard.errors },
          status: :unprocessable_entity
      end
    end
    
    def index
      if params[:search].present?
        @dashboards = @dashboards.where("LOWER(dashboards.name) ILIKE :search OR LOWER(dashboards.sku) ILIKE :search", search: "%#{params[:search]}%")
      end
      dashboards = @dashboards
      render json: BxBlockDashboard::DashboardSerializer.new(dashboards).serializable_hash, status: :ok
    end

   

    def find_dashboards
      @dashboards = BxBlockDashboard::Dashboard.all
    end
   
    private

    def dashboard_params
      params.require(:data).permit(:sku,:name,:tag,:category,:in_stock,:available_stock)
    end

  end
end
