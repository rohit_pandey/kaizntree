module BxBlockLogin
  class LoginsController < ApplicationController
    def create
      case params[:data][:type] #### rescue invalid API format
      when 'email_account'
        account = OpenStruct.new(jsonapi_deserialize(params))
        account.type = params[:data][:type]

        output = AccountAdapter.new

        output.on(:account_not_found) do |account|
          render json: {
            error: {
              message: 'Account not found, or not activated',
            },
          }, status: :unprocessable_entity
        end

        output.on(:failed_login) do |account|
          render json: {
            error: {
              message: 'Login Failed',
            },
          }, status: :unauthorized
        end

        output.on(:successful_login) do |account, token, refresh_token, login_type|
          
          if login_type == 'email_account'
            email_otp = AccountBlock::EmailOtp.new(email: account.email)
            if email_otp.save
              send_email_for email_otp
              render json: serialized_email_otp(email_otp, account.id),
                status: :created
            else
              render json: {
                error: {message: email_otp.errors.full_messages.slice(0)},
              }, status: :unprocessable_entity
            end
          end
        end

        output.login_account(account)
      else
        render json: {
          error: {
            message: 'Invalid Account Type',
          },
        }, status: :unprocessable_entity
      end
    end

    private

    def send_email_for(otp_record)
      # BxBlockForgotPassword::EmailOtpMailer
      #   .with(otp: otp_record, host: request.base_url)
      #   .otp_email.deliver
    end

    def serialized_email_otp(email_otp, account_id)
      token = token_for(email_otp, account_id)
      BxBlockForgotPassword::EmailOtpSerializer.new(
        email_otp,
        meta: { token: token, account_id: account_id }
      ).serializable_hash
    end

    def token_for(otp_record, account_id)
      BuilderJsonWebToken.encode(
        otp_record.id,
        #10.minutes.from_now,
        24.hours.from_now,
        type: otp_record.class,
        account_id: account_id
      )
    end
  end
end
