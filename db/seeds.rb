# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
c = AdminUser.create_or_find_by(email: "mangephone@gmail.com", password: "password")
AccountBlock::Tier.find_or_create_by(unique_name: "tier1",name: "tier1")
AccountBlock::Tier.find_or_create_by(unique_name: "tier2",name: "tier2")
AccountBlock::Tier.find_or_create_by(unique_name: "tier3",name: "tier3")