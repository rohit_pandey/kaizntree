class CreateAccountBlockUpdatedTiers < ActiveRecord::Migration[6.0]
  def change
    create_table :updated_tiers do |t|
      t.string :name
      t.integer :tier_id
      t.integer :user_id

      t.timestamps
    end
  end
end
