class CreateBxBlockDashboardDashboards < ActiveRecord::Migration[6.0]
  def change
    create_table :dashboards do |t|
      t.string :sku
      t.string :name
      t.bigint :in_stock
      t.bigint :available_stock
      t.integer :tag
      t.integer :category

      t.timestamps
    end
  end
end
